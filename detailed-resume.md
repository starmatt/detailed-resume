## Matthieu Borde, développeur web fullstack

**Adresse e-mail :** contact@starmatt.net

**Téléphone :** +33 6 71 41 21 18

**Site internet :** https://starmatt.net | https://gitlab.com/starmatt

**Anglais parlé et écrit**

---

**Outils :**
- `Linux`, `Docker`, `Git`
- `VIM`, `SublimeText`
- `MariaDB/MySQL`
- `Phpstan`, `eslint`, `prettier`
- `Composer`, `Yarn`, `NPM`
- `Webpack`, `Vite`
- `Apache`, `nginx`
- `GIMP`, `Photoshop`, `Inkscape`

**Languages et Frameworks :**
 - `PHP` : `Laravel`, `PHPUnit`, `Pest`
 - `Javascript` : `Nodejs`, `Express`, `Vuejs 2/3`, `React (API fonctionnelle)`, `Nuxtjs`, `Nextjs`, `Jest`
 - `CSS`: `Sass`, `Tailwind`, `Bulma`, `Bootstrap`
 - `Lua`

**Compétences :** Backend, Frontend, Programmation Orientée Objet, Programmation fonctionnelle, MVC, RESTful APIs, Micro services, Rédaction de documentation

## R2D2 (Développeur web fullstack)

*Juillet 2021 - Présent*

---

### Observia

Observia offre des solutions e-santé, facilitant la mise en relation et le suivi patient/médecin.

**Environnement technique :** `PHP`, `Laravel`, `Javascript`, `Vuejs`, `Webpack`, `Vite`, `MariaDB`, `nginx`, `PHPUnit`, `Docker`

- Apport d'expertise sur une dizaine de projets différents.
- Amélioration, modernisation et implémentation de nouvelles features sur des projets *legacy*.
- Amorçage de nouveaux projets:
  - Architecture de la structure des bases de données.
  - Intégration du design system.
  - Intégration des outils front-end et back-end.
  - Développement sur chaque aspect de la codebase.
- Lead technique sur de nouveaux projets:
  - Mise en place de systèmes d'authentification à clients multiples.
  - Mise en place de bonnes pratiques: tests automatiques, outils d'analyse statique, norme syntaxique, feature grooming, etc.
  - Révision de code en groupe.
  - Peer-programming.
- Prise de parole et présentations techniques.

## Multipass (Développeur web fullstack)

*Janvier 2018 - Septembre 2018*

---

### Agora

Service qui permet aux clients d'implémenter simplement les normes RGPD sur leurs plateformes. Projet réalisé en équipe.

**Objectifs**
- Création d'un MVP
- Développement d'une techonologie SSO (single sign on)
- Architecture de la BDD
- Tests unitaires

**Environnement technique :** `PHP`, `Laravel`, `MariaDB`, `nginx`, `PHPUnit`

---

### Multipass

Multipass est un service qui permet aux utilisateurs d'avoir accès au contenu premium et sans pubs des média partenaires avec un abonnement unique. Projet réalisé en équipe.

**Objectifs :**
- Développement de différents *plug-ins* pour Laravel, Wordpress, Symphony, etc. utilisés par les applications web des clients
- Création d'une bibliothèque de fonctionnalités communes aux différents plug-ins en PHP
    - Documentation
- Développement d'une techonologie SSO (single sign on)
- Optimisation SEO de multipass.net
- Implémentation des normes RGPD
    - Chiffrement de la base de donnée
    - Création d'un système opt-in pour la collecte des données des utilisateurs
- Tests unitaires

**Environnement technique :** `PHP`, `Laravel`, `JS`, `CSS`, `MariaDB`, `nginx`, `PHPUnit`


## Net&Work (Développeur web fullstack)

*Septembre 2016 - Janvier 2018*

---

### Before Shopping

Application web de *Cashback*. Projet réalisé seul.

**Objectifs :**
- Architecture de la BDD
- Mise en place de systèmes utilisateurs
    - Inscription/Connexion
    - Profil, parrainage
    - Porte-feuille virtuel
- Création d'une bibliothèque pour faciliter le traitement de données provenants des APIs des régies publicitaires partenaires
    - Documentation
- Création d'un moteur de recherche et filtrage par marques, catégories, centres d'intérêts
- Intégration d'un design donné
- Création d'une interface dynamique
- Création d'un backoffice administrateur
    - Rôles et permissions
    - Modération
    - Traitement des données de la BDD
- Création de pages responsives
- Mise en place de l'internationalisation de l'application
    - Traduction
- Optimisation SEO, accessibilité des utilisateurs
- Tests unitaires

**Environnement technique :** `PHP`, `Laravel`, `JS`, `Vuejs`, `Webpack`, `CSS`, `Sass`, `MariaDB`, `nginx`, `PHPUnit`

---

### Codes-de-Réduction (WebExtension)

WebExtension affichant de manière automatique des codes de réduction à la naviguation sur des sites marchands. Projet réalisé seul.

**Objectifs :**
- Création d'une API pour fournir les données nécessaires à l'extension
- Affichage des données adéquates en fonction de la navigation des utilisateurs

**Environnement technique :** `JS`, `CSS`

---

### Codes-de-Réduction

Annuaire de codes de réduction en ligne. Projet réalisé en équipe.

**Objectifs :**
- Migration de l'application de Wordpress vers Laravel
- Création de scripts pour migrer les données de Wordpress vers une BDD sur mesure
- Création d'un moteur de recherche
- Mise en place de systèmes utilisateurs
    - Inscription/Connexion
- Création d'un backoffice administrateur
    - Rôles et permissions
    - Modération
    - Traitement des données de la BDD
- Intégration d'un design donné

**Environnement technique :** `PHP`, `Laravel`, `Wordpress`, `JS`, `CSS`, `nginx`

---

### netandwork.fr

Identité visuelle et présentation de la société. Projet réalisé seul.

**Objectifs :**
- Intégration d'un design donné
- Création de pages responsives
- Mise en place d'un formulaire de contact
- Mise en place de l'internationalisation de l'application
- Optimisation SEO, accessibilité des utilisateurs

**Environnement technique :** `PHP`, `JS`, `jQuery`, `CSS`, `nginx`


## Net&Work (Stage en développement web)

*Avril 2016 - Septembre 2016*

---

### Tabtroop

Bibliothèque de tablatures en ligne pour divers instruments de musique. Projet réalisé en équipe.

**Objectifs :**
- Architecture de la BDD
- Mise en place de systèmes utilisateurs
    - Inscription/Connexion
    - Commentaires/Réponses
    - Sousmission et validation de données
- Implémentation d'un algorithme de reconnaissance d'accords dans une tablature textuelle
- Création d'un backoffice administrateur :
    - Rôles et permissions
    - Modération
    - Éditeur de tablature
    - Traitement des données de la BDD
- Intégration d'un design donné
- Création de pages responsives

**Environnement technique :** `PHP`, `Laravel`, `JS`,  `jQuery`, `CSS`, `MySQL`, `nginx`

---

### contacter.com

Annuaire d'entreprises en ligne. Projet réalisé seul.

**Objectifs :**
- Refonte du moteur de recherche
- Mise en place de la localisation GPS et IP des utilisateurs
- Amélioration du SEO
- Amélioration des vues mobiles

**Environnement technique :** `PHP`, `Laravel`, `JS`,  `jQuery`, `CSS`, `MySQL`, `nginx`

## Projets personnels

### starmatt.net

Portfolio. Projet réalisé seul.

**Objectifs :**
- Création du design
- Mise en place de l'internationalisation
    - Traduction
- Création d'un formulaire de contact
- Optimisation SEO, accessibilité des utilisateurs

**Environnement technique :** `JS`, `Express`, `Vuejs`, `Nuxtjs`, `CSS`, `Tailwind`

---

### Prends-en de la Graine

Plateforme en ligne d'échange de semences paysannes. Projet réalisé en équipe.

**Objectifs :**
- Création de l'identité visuelle
- Mise en place d'une norme régissant le travail en équipe
- Création d'une interface dynamique basée sur des composants réutilisables
- Création d'une interface de recherche basée sur un système de filtres
- Création d'un système de notification
- Création de systèmes utilisateurs
    - Inscription/Connexion
    - Profil paramétrable
    - Inventaire
    - Partage sur les réseaux sociaux
- Optimisation SEO, accessibilité des utilisateurs

**Environnement technique :** `JS`, `Nodejs`, `React`, `Nextjs`, `Apollo`, `GraphQL`, `CSS`, `Sass`, `Webpack`

---

### Youtube FTW

WebExtension Firefox/Chromium permettant d'ouvrir les vidéos youtube à la taille de votre navigateur et d'obstruer le reste de l'UI. Projet réalisé seul.

**Objectifs :**
- Détection automatique des pages appropriées
- Fonctionnalités minimales
    - Recadrement du player
    - Détection du mode théâtre
    - Positionnement du bouton consistent
- Implémentation des paramètres de l'extension dans une pop-up
- Optimisation des performances
- Déploiement de l'extension sur addons.mozilla.org

**Environnement technique :** `JS`, `CSS`, `Webpack`, `Babel`

## École 42 (Formation de développeur)

*Août 2014 - Mars 2020*

Spécialisé dans les techonologies web.

---

### Matcha

Application web de rencontres. Projet de groupe.

**Objectifs :**
- Architecture de la BDD
- Mise en place de systèmes utilisateurs
    - Inscription/Connexion
    - Commentaires
    - Sousmission et validation de données
    - Mise en relation des utilisateurs
- Création d'un design et d'une interface dynamique

**Environnement technique :** `Nodejs`, `Express`, `Vuejs`, `CSS`, `MariaDB`

---

###  Camagru

Application web de prise de photos (via webcam) et application de filtres. Projet réalisé seul.

**Objectifs :**
- Création d'un mini-framework MVC en PHP
    - Routeur
    - Controlleurs
    - ORM
    - Modèles
    - Validateur de données
- Architecture de la BDD
- Mise en place de systèmes utilisateurs
    - Inscription/connexion
    - Commentaires
    - Sousmission et validation de données
- Traitement d'images en PHP via la lib `GD`
- Création d'un design et d'une interface dynamique

**Environnement technique :** `PHP`, `JS`, `CSS`, `MariaDB`
